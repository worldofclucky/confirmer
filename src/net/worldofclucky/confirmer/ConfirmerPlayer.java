package net.worldofclucky.confirmer;

import java.util.ArrayList;
import java.util.Timer;
import java.util.UUID;

import net.canarymod.Canary;

public class ConfirmerPlayer {
	private UUID uuid;
	private ArrayList<ConfirmableCommand> pendingCommands;
	
	protected ConfirmerPlayer(UUID uuid) {
		this.uuid = uuid;
		pendingCommands = new ArrayList<ConfirmableCommand>();
	}
	
	public ArrayList<ConfirmableCommand> getConfirmableCommands() {
		return pendingCommands;
	}
	
	public ConfirmableCommand getConfirmableCommand(String commandName) {
		commandName = commandName.toLowerCase();
		for (ConfirmableCommand c : pendingCommands) {
			if (c.matchesCommandNameHash(commandName.hashCode())) {
				return c;
			}
		}
		return null;
	}

	public ConfirmableCommand getLatestConfirmableCommand() {
		if (!pendingCommands.isEmpty()) {
			return pendingCommands.get(0);
		} else {
			return null;
		}
	}
	
	public UUID getUUID() {
		return uuid;
	}
	
	public boolean addConfirmableCommand(ConfirmableCommand confirmableCommand) {
		if (confirmableCommand != null) {
			pendingCommands.add(confirmableCommand);
			return true;
		}
		return false;
	}
	
	public boolean removeConfirmableCommand(String commandName) {
		commandName = commandName.toLowerCase();
		for (ConfirmableCommand c : pendingCommands) {
			if (c.matchesCommandNameHash(commandName.hashCode())) {
				return removeConfirmableCommand(c);
			}
		}
		return false;
	}
	
	public boolean removeConfirmableCommand(ConfirmableCommand confirmableCommand) {
		if (pendingCommands.remove(confirmableCommand)) {
			if (pendingCommands.size() == 0) {
				Confirmer.getConfirmerPlayerManager().disconnect(uuid);
			}
			return true;
		}
		return false;
	}
	
	private void message(String... string) {
		Canary.getServer().getPlayerFromUUID(uuid).message(string);
	}
	
	protected boolean createCommand(ConfirmableCommand confirmableCommand) {
		if (addConfirmableCommand(confirmableCommand)) {
			Timer timer;
			timer = new Timer();
			timer.schedule(new TimeoutTimer(this, confirmableCommand), confirmableCommand.getTimeoutTime() * 1000);
			message(confirmableCommand.getCreationMessage());
			return true;
		} else {
			message(confirmableCommand.getErrorMessage());
			return false;
		}
	}
	
	protected boolean confirmCommand(ConfirmableCommand confirmableCommand) {
		if (confirmableCommand.confirm()) {
			message(confirmableCommand.getConfirmMessage());
			removeConfirmableCommand(confirmableCommand);
			return true;
		} else {
			message(confirmableCommand.getErrorMessage());
			return false;
		}
	}
	
	protected boolean denyCommand(ConfirmableCommand confirmableCommand) {
		if (confirmableCommand.deny()) {
			message(confirmableCommand.getDenyMessage());
			removeConfirmableCommand(confirmableCommand);
			return true;
		} else {
			message(confirmableCommand.getErrorMessage());
			return false;
		}
	}
	
	protected boolean timeoutCommand(ConfirmableCommand confirmableCommand) {
		if (confirmableCommand.timeout()) {
			message(confirmableCommand.getTimeoutMessage());
			removeConfirmableCommand(confirmableCommand);
			return true;
		} else {
			message(confirmableCommand.getErrorMessage());
			return false;
		}
	}
	
	
}
