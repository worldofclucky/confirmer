package net.worldofclucky.confirmer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.canarymod.api.entity.living.humanoid.Player;

public class ConfirmerPlayerManager {
	private ArrayList<ConfirmerPlayer> players;
	
	protected ConfirmerPlayerManager() {
		players = new ArrayList<ConfirmerPlayer>();
	}
	
	protected ConfirmerPlayer connect(UUID uuid) {
		ConfirmerPlayer player = new ConfirmerPlayer(uuid);
		players.add(player);
		return player;
	}
	
	protected void disconnect(UUID uuid) {
		players.remove(getPlayer(uuid));
	}
	
	/**
	 * <code>public ConfirmerPlayer getPlayer(UUID uuid)</code>
	 * <br /> <br />
	 * Retrieves the ConfirmerPlayer with the provided uuid. If a ConfirmerPlayer does not yet exist, one will be created and returned.
	 * 
	 * @param uuid the UUID of the player needing to be gotten
	 * @return The ConfirmerPlayer with the provided UUID, or null if one can not be created
	 */
	public ConfirmerPlayer getPlayer(UUID uuid) {
		for (ConfirmerPlayer p : players) {
			if (uuid == p.getUUID()) {
				return p;
			}
		}
		
		return connect(uuid);
	}
	
	/**
	 * <code>public ConfirmerPlayer getPlayer(Player player)</code>
	 * <br /> <br />
	 * Retrieves the ConfirmerPlayer with the provided uuid. If a ConfirmerPlayer does not yet exist, one will be created and returned.
	 * 
	 * @param player the player needing to be gotten
	 * @return The ConfirmerPlayer with the provided Player, or null if one can not be created
	 */
	public ConfirmerPlayer getPlayer(Player player) {
		return getPlayer(player.getUUID());
	}
	
	public List<ConfirmerPlayer> getPlayers() {
		return players;
	}
}
