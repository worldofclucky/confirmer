package net.worldofclucky.confirmer;

import java.util.UUID;

import net.canarymod.Canary;
import net.canarymod.api.entity.living.humanoid.Player;
import net.canarymod.chat.MessageReceiver;
import net.canarymod.chat.ReceiverType;

public abstract class ConfirmableCommand /*extends CanaryCommand*/ {
	
	private String commandName;
	private int commandNameHash;
	protected Integer timeoutTime;
	protected String confirmMessage;
	protected String denyMessage;
	protected String timeoutMessage;
	protected String creationMessage;
	protected String errorMessage;
	
//	public ConfirmableCommand(Command meta, CommandOwner owner,
//			LocaleHelper translator, TabCompleteDispatch tabComplete) {
//		super(meta, owner, translator, tabComplete);
//		// TODO Auto-generated constructor stub
//	}
//	
//	public ConfirmableCommand(Command meta, CommandOwner owner,
//			LocaleHelper translator) {
//		super(meta, owner, translator);
//		// TODO Auto-generated constructor stub
//	}
	
	public UUID getCallingPlayerUUID() {
		if (commandName != null) {
			for (ConfirmerPlayer p : Confirmer.getConfirmerPlayerManager().getPlayers()) {
				if (p.getConfirmableCommand(commandName) == this) {
					return p.getUUID();
				}
			}
		}
		return null;
	}
	
	public Player getCallingPlayer() {
		UUID uuid = getCallingPlayerUUID();
		if (uuid != null) {
			return Canary.getServer().getPlayerFromUUID(uuid);
		}
		return null;
	}
	
	public String getCommandName() {
		return commandName;
	}
	
	public boolean matchesCommandNameHash(int hashCode) {
		if (hashCode == commandNameHash) {
			return true;
		} else {
			return false;
		}
	}
	
	public int getTimeoutTime() {
		if (timeoutTime != null) {
			return timeoutTime.intValue();
		} else {
			return Confirmer.getConfigManager().getDefaultTimeoutTime();
		}
	}
	
	public String getConfirmMessage() {
		if (confirmMessage != null) {
			return confirmMessage;
		} else {
			return Confirmer.getConfigManager().getDefaultConfirmMessage().replace("<command_name>", commandName);
		}
	}
	
	public String getDenyMessage() {
		if (denyMessage != null) {
			return denyMessage;
		} else {
			return Confirmer.getConfigManager().getDefaultDenyMessage().replace("<command_name>", commandName);
		}
	}
	
	public String getTimeoutMessage() {
		if (timeoutMessage != null) {
			return timeoutMessage;
		} else {
			return Confirmer.getConfigManager().getDefaultTimeoutMessage().replace("<command_name>", commandName);
		}
	}
	
	public String getCreationMessage() {
		if (creationMessage != null) {
			return creationMessage;
		} else {
			return Confirmer.getConfigManager().getDefaultCreateMessage().replace("<command_name>", commandName);
		}
	}
	
	public String getErrorMessage() {
		if (errorMessage != null) {
			return errorMessage;
		} else {
			return Confirmer.getConfigManager().getDefaultErrorMessage().replace("<command_name>", commandName);
		}
	}
	
//	/**
//	 * Creates a ConfirmableCommand instance for the player.
//	 * 
//	 * @param command The command which is needing to be confirmed (only the first word (ex. groupmod))
//	 * @param player The Player which is issuing the command
//	 * @return True if the creation process is successful, otherwise false
//	 */
//	public boolean create(String command, Player player) {
//		commandName = command.toLowerCase();
//		commandNameHash = commandName.hashCode();
//		
//		ConfirmerPlayer confirmerPlayer = Confirmer.getConfirmerPlayerManager().getPlayer(player);
//		if (confirmerPlayer != null) {
//			return confirmerPlayer.createCommand(this);
//		}
//		return false;
//	}
	
	/**
	 * Creates a ConfirmableCommand instance for the caller.
	 * If the caller is anything except for a Player object, then the command will be immediately confirmed.
	 * 
	 * @param command The command which is needing to be confirmed (only the first word (ex. groupmod))
	 * @param caller The MessageReciever which is issuing the command
	 * @return True if the creation process is successful, otherwise false
	 */
	public boolean create(String command, MessageReceiver caller) {
		commandName = command.toLowerCase();
		commandNameHash = commandName.hashCode();
		
		if (caller.getReceiverType() == ReceiverType.PLAYER) {
			if (caller.hasPermission("confirmer.bypass")) {
				if (this.confirm()) {
					caller.message(this.getConfirmMessage());
					return true;
				} else {
					caller.message(this.getErrorMessage());
					return false;
				}
			}
			ConfirmerPlayer confirmerPlayer = Confirmer.getConfirmerPlayerManager().getPlayer(caller.asPlayer());
			return confirmerPlayer.createCommand(this);
		} else if (caller.getReceiverType() != null) {
			if (this.confirm()) {
				caller.message(this.getConfirmMessage());
				return true;
			} else {
				caller.message(this.getErrorMessage());
				return false;
			}
		} else {
			return false;
		}
	}
	
	public abstract boolean confirm();
	public abstract boolean deny();
	public abstract boolean timeout();
}
