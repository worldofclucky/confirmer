package net.worldofclucky.confirmer;

import java.util.TimerTask;

public class TimeoutTimer extends TimerTask {
	
	ConfirmableCommand command;
	ConfirmerPlayer confirmerPlayer;

	protected TimeoutTimer(ConfirmerPlayer confirmerPlayer, ConfirmableCommand command) {
		this.confirmerPlayer = confirmerPlayer;
		this.command = command;
	}
	
	@Override
	public void run() {
		if (confirmerPlayer.getConfirmableCommands().contains(command)) {
			confirmerPlayer.timeoutCommand(command);
		}
	}

}
