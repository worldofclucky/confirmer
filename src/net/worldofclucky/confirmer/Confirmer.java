package net.worldofclucky.confirmer;

import net.canarymod.Canary;
import net.canarymod.commandsys.CommandDependencyException;
import net.canarymod.logger.Logman;
import net.canarymod.plugin.Plugin;

public class Confirmer extends Plugin {
	public static Logman logger;
	private static Plugin instance;
	private static ConfirmerPlayerManager playerManager;
	private static ConfigManager configManager;
	
	public Confirmer() {
		logger = getLogman();
		instance = this;
	}

	@Override
	public void disable() {
		
	}

	@Override
	public boolean enable() {
		playerManager = new ConfirmerPlayerManager();
		configManager = new ConfigManager();
		
		try {
			Canary.commands().registerCommands(new ConfirmerCommands(), this, false);
		} catch (CommandDependencyException e) {
			logger.error("Failure to load Confirmer commands: confirm/deny");
			e.printStackTrace();
		}
		
		return true;
	}
	
	public static ConfirmerPlayerManager getConfirmerPlayerManager() {
		return playerManager;
	}
	
	public static ConfigManager getConfigManager() {
		return configManager;
	}
	
	public static Plugin getInstance() {
		return instance;
	}
}
