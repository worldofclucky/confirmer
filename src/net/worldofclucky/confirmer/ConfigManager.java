package net.worldofclucky.confirmer;

import net.canarymod.chat.ChatFormat;
import net.canarymod.config.Configuration;
import net.visualillusionsent.utils.PropertiesFile;

public class ConfigManager {
	private PropertiesFile config;
	
	public ConfigManager() {
		config = Configuration.getPluginConfig(Confirmer.getInstance());
	}
	
	public String getDefaultConfirmMessage() {
		return config.getString("Default Confirm Message",ChatFormat.GREEN.toString() + "The pending action for <command_name> has been confirmed");
	}
	public String getDefaultDenyMessage() {
		return config.getString("Default Deny Message",ChatFormat.GREEN.toString() + "The pending action for <command_name> has been denied");
	}
	public String getDefaultCreateMessage() {
		return config.getString("Default Create Message",ChatFormat.GREEN.toString() + "Type /confirm <command_name> to proceed with this action");
	}
	public String getDefaultTimeoutMessage() {
		return config.getString("Default Timeout Message",ChatFormat.GREEN.toString() + "The pending action for <command_name> has timed out");
	}
	public String getDefaultErrorMessage() {
		return config.getString("Default Error Message",ChatFormat.RED.toString() + "There was an unknown error during the execution of <command_name>");
	}
	
	public int getDefaultTimeoutTime() {
		return config.getInt("Default Timeout Time",60);
	}
}
