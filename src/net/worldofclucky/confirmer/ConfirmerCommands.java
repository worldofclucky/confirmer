package net.worldofclucky.confirmer;

import net.canarymod.api.entity.living.humanoid.Player;
import net.canarymod.chat.ChatFormat;
import net.canarymod.chat.MessageReceiver;
import net.canarymod.chat.ReceiverType;
import net.canarymod.commandsys.Command;
import net.canarymod.commandsys.CommandListener;

public class ConfirmerCommands implements CommandListener {
	
	@Command(aliases = "confirm",
			description = "Confirm a prompted request. Specify command name to confirm a specific pending request.",
			toolTip = "/confirm [command name]",
			permissions = "",
			max = 2)
	public void Confirm(MessageReceiver caller, String[] args) {
		if (caller.getReceiverType() != ReceiverType.PLAYER) {
			caller.message(ChatFormat.RED + "This command can only be performed by a player!");
			return;
		}
		
		Player player = caller.asPlayer();
		ConfirmerPlayer cPlayer = Confirmer.getConfirmerPlayerManager().getPlayer(player);
		if (cPlayer == null) return;
		
		if (args.length < 2) {
			ConfirmableCommand confirmableCommand = cPlayer.getLatestConfirmableCommand();
			if (confirmableCommand != null) {
				cPlayer.confirmCommand(confirmableCommand);
			} else {
				caller.message(ChatFormat.RED + "No pending actions were found.");
			}
		} else {
			ConfirmableCommand confirmableCommand = cPlayer.getConfirmableCommand(args[1]);
			if (confirmableCommand != null) {
				cPlayer.confirmCommand(confirmableCommand);
			} else {
				caller.message(ChatFormat.RED + "Pending action not found. Please check your spelling for the command name.");
			}
		}
	}
	
	@Command(aliases = "deny",
			description = "Deny a recent prompted request. Specify command name to deny a specific pending request.",
			toolTip = "/deny [command name]",
			permissions = "")
	public void Deny(MessageReceiver caller, String[] args) {
		if (caller.getReceiverType() != ReceiverType.PLAYER) {
			caller.message(ChatFormat.RED + "This command can only be performed by a player!");
			return;
		}
		
		Player player = caller.asPlayer();
		ConfirmerPlayer cPlayer = Confirmer.getConfirmerPlayerManager().getPlayer(player);
		if (cPlayer == null) return;
		
		if (args.length < 2) {
			ConfirmableCommand confirmableCommand = cPlayer.getLatestConfirmableCommand();
			if (confirmableCommand != null) {
				cPlayer.denyCommand(confirmableCommand);
			} else {
				caller.message(ChatFormat.RED + "No pending actions were found.");
			}
		} else {
			ConfirmableCommand confirmableCommand = cPlayer.getConfirmableCommand(args[1]);
			if (confirmableCommand != null) {
				cPlayer.denyCommand(confirmableCommand);
			} else {
				caller.message(ChatFormat.RED + "Pending action not found. Please check your spelling for the command name.");
			}
		}
	}
	
}
